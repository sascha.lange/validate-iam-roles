package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"
	"sync"

	"github.com/aws/aws-sdk-go/aws/session"

	"github.com/aws/aws-sdk-go/service/iam"
	"github.com/schollz/progressbar"
	ccsv "github.com/tsak/concurrent-csv-writer"
)

var csvHeader = []string{
	"Account",
	"Missing IAM Role",
}

func main() {
	// Check command line flags
	assumeRoleArg := flag.String("role", "OrganizationAccountAccessRole", "AWS assumeRole name")
	accountArg := flag.String("account", "", "AWS account id")
	blacklistArg := flag.String("blacklist", "", "Provide blacklist name")
	outputfileArg := flag.String("outpout", "result.csv", "Changes the name of the created csv file")
	profileArg := flag.String("profile", "", "AWS profile name for authentication")
	inputFileArg := flag.String("i", "", "Provide input list")

	flag.Parse()

	// Create and validate session
	sess := createSession(*profileArg)
	_, err := sess.Config.Credentials.Get()
	checkError("Error reading credentials\n", err)

	myInputlist := readIAMRolesFromFile(*inputFileArg)
	checkError("Cannot read input list\n", err)

	myBlacklist := readBlacklist(*blacklistArg)
	checkError("Error reading blacklist\n", err)

	myAccounts := createAccountList(sess, *accountArg)
	fmt.Printf("Number of discovered sub accounts: %d\n", len(myAccounts)-1)

	fmt.Println("Start processing ...")
	bar := progressbar.New(len(myInputlist) * len(myAccounts))

	currentAccountID := getCurrentAccountID(sess)

	var wg sync.WaitGroup
	wg.Add(len(myAccounts))

	csv, err := ccsv.NewCsvWriter(*outputfileArg)
	checkError("Cannot create output file\n", err)
	defer csv.Close()
	csv.Write(csvHeader)

	// Prepare error.log
	errLog, err := os.Create("error.log")
	checkError("Cannot create error.log", err)
	defer errLog.Close()
	var errMu sync.Mutex

	// Global error state
	isErr := false

	for _, account := range myAccounts {
		bar.Add(1)
		if accountBlacklisted(account, myBlacklist) {
			wg.Done()
			continue
		}
		go func(account string, csv *ccsv.CsvWriter) {
			defer wg.Done()

			bar.Add(1)
			newSess := assumeRoleOnSubAccount(sess, currentAccountID, account, *assumeRoleArg, constRegion)
			_, err := newSess.Config.Credentials.Get()
			if err != nil {
				isErr = true
				accessErr := fmt.Sprintf("Cannot login to account %s in region %s\n", account, constRegion)
				message := fmt.Sprintf("%s%v\n\n", accessErr, err.Error())
				writeErrorLog(message, errLog, &errMu)
				return
			}

			configuredRoles := getRoles(newSess)
			for _, role := range myInputlist {
				bar.Add(1)
				if !validateRole(role, configuredRoles) {
					csv.Write([]string{
						account,
						role,
					})
				}
			}
		}(account, csv)
	}
	wg.Wait()
	errorInfo(isErr)
}

// getRoles returns a map of discovered IAM roles.
// Key is the name of the role in lowercase.
// Value is the original name of the role.
func getRoles(sess *session.Session) map[string]string {
	iamSvc := iam.New(sess)
	roles, err := iamSvc.ListRoles(&iam.ListRolesInput{})
	checkError("Cannot read iam roles", err)

	result := make(map[string]string)
	for i := 0; i < len(roles.Roles); i++ {
		result[strings.ToLower(*roles.Roles[i].RoleName)] = *roles.Roles[i].RoleName
	}

	return result
}

func validateRole(r string, m map[string]string) bool {
	if _, ok := m[strings.ToLower(r)]; ok {
		return true
	}
	return false
}

func readIAMRolesFromFile(path string) []string {
	var lines []string
	if path == "" {
		fmt.Printf("Input list required!\n")
		os.Exit(2)
	}

	if len(path) != 0 {
		file, err := os.Open(path)
		checkError("Cannot open blacklist\n", err)
		defer file.Close()
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			lines = append(lines, scanner.Text())
		}
	}
	return lines
}
