package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sync"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/organizations"
	"github.com/aws/aws-sdk-go/service/sts"
)

// Default aws region for api calls that need a region
const constRegion = "eu-west-1"

func errorInfo(isErr bool) {
	if isErr {
		fmt.Printf("\nErrors during run! Check your error.log\n")
	}
}

func writeErrorLog(m string, f *os.File, w *sync.Mutex) {
	w.Lock()
	defer w.Unlock()

	f.WriteString(m)
	f.Sync()
}

func accountBlacklisted(account string, blacklist []string) bool {
	var result bool
	for _, element := range blacklist {
		if element == account {
			result = true
			break
		}
	}
	return result
}

func readBlacklist(path string) []string {
	var lines []string
	if len(path) != 0 {
		file, err := os.Open(path)
		checkError("Cannot open blacklist\n", err)
		defer file.Close()
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			lines = append(lines, scanner.Text())
		}
	}
	return lines
}

func assumeRoleOnSubAccount(sess *session.Session, currentAcc string, acc string, role string, reg string) *session.Session {
	var newSess *session.Session
	if currentAcc != acc {
		roleToAssumeArn := fmt.Sprintf("arn:aws:iam::%v:role/%v", acc, role)
		creds := stscreds.NewCredentials(sess, roleToAssumeArn)
		newSess = sess.Copy(&aws.Config{
			Region:      aws.String(reg),
			Credentials: creds,
		})
	} else {
		newSess = sess.Copy(&aws.Config{
			Region: aws.String(reg),
		})
	}
	return newSess
}

func createRegionList(sess *session.Session, region string) []string {
	var regions []string
	if len(region) <= 0 {
		regions = getRegions(sess)
		return regions
	}
	return []string{region}
}

func createSession(profile string) *session.Session {
	var sess *session.Session
	if len(profile) <= 0 {
		sess = session.Must(session.NewSession(&aws.Config{
			Region: aws.String(constRegion),
		}))
		return sess
	}
	sess = session.Must(session.NewSessionWithOptions(session.Options{
		Config:  aws.Config{Region: aws.String(constRegion)},
		Profile: profile,
	}))
	return sess
}

func createAccountList(sess *session.Session, account string) []string {
	if len(account) <= 0 {
		return getOrganizationAccounts(sess)
	}
	return []string{account}
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

func getRegions(sess *session.Session) []string {
	ec2Svc := ec2.New(sess)
	regions, err := ec2Svc.DescribeRegions(&ec2.DescribeRegionsInput{})
	checkError("Cannot get regions\n", err)
	var result []string
	for _, region := range regions.Regions {
		result = append(result, *region.RegionName)
	}
	return result
}

func getCurrentAccountID(sess *session.Session) string {
	stsSvc := sts.New(sess)
	idendtity, err := stsSvc.GetCallerIdentity(&sts.GetCallerIdentityInput{})
	checkError("Cannot get main account details\n", err)
	return *idendtity.Account
}

func getOrganizationAccounts(sess *session.Session) []string {
	orgSvc := organizations.New(sess)
	orgAccounts, err := orgSvc.ListAccounts(&organizations.ListAccountsInput{})
	checkError("Cannot get organization accounts\n", err)
	token := orgAccounts.NextToken
	var accounts []string
	for _, account := range orgAccounts.Accounts {
		accounts = append(accounts, *account.Id)
	}

	for token != nil {
		moreOrgAccounts, err := orgSvc.ListAccounts(&organizations.ListAccountsInput{
			NextToken: token,
		})
		checkError("Cannot fetch additional organization accounts\n", err)
		token = moreOrgAccounts.NextToken
		for _, account := range moreOrgAccounts.Accounts {
			accounts = append(accounts, *account.Id)
		}
	}
	return accounts
}
