.PHONY: build clean

build:
	go get github.com/aws/aws-sdk-go
	go get github.com/tsak/concurrent-csv-writer
	go get github.com/schollz/progressbar
	go get github.com/mitchellh/colorstring
	env GOOS=linux go build -ldflags="-s -w" -o  bin/validate-iam-role-linux-amd64 *.go
	upx bin/validate-iam-role-linux-amd64
	env GOOS=darwin go build -ldflags="-s -w" -o  bin/validate-iam-role-darwin-amd64 *.go
	upx bin/validate-iam-role-darwin-amd64
	env GOOS=windows go build -ldflags="-s -w" -o  bin/validate-iam-role-windows-amd64.exe *.go
	upx bin/validate-iam-role-windows-amd64.exe

clean:
	rm -rf ./bin
